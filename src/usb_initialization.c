/*
 * usb_initialization.c
 *
 *  Created on: Nov 20, 2014
 *      Author: denismasyukov
 */

#include "usb_initialization.h"
#include "usb_definitions.h"
#include "delay_routines.h"
#include "usb_routines.h"

void setupUsb(void) {
	// --- Switch system clock to 48 Mhz (we will use USB!!) ---
	// do not bypass system oscillator
	LPC_SYSCTL->SYSOSCCTRL &= ~(1 << 0);

	// wake up system oscillator
	LPC_SYSCTL->PDRUNCFG &= ~(1 << 5);
	delayMicroseconds(200);

	// use system oscillator in PLL
	LPC_SYSCTL->SYSPLLCLKSEL = 1;
	LPC_SYSCTL->SYSPLLCLKUEN = 0;
	LPC_SYSCTL->SYSPLLCLKUEN = 1;

	// power down PLL for now
	LPC_SYSCTL->PDRUNCFG |= (1 << 7);

	// set M=4 and P=2 for system PLL
	LPC_SYSCTL->SYSPLLCTRL |= (0x3 | (0x1 << 5));

	// awake system PLL
	LPC_SYSCTL->PDRUNCFG &= ~(1 << 7);
	while ((LPC_SYSCTL->SYSPLLSTAT & 1) == 0) {}

	// Update the clock source by setting 1 in SYSPLLUEN register (see Table 19) register, and wait until clock source is updated
	LPC_SYSCTL->SYSPLLCLKUEN = 0;
	LPC_SYSCTL->SYSPLLCLKUEN = 1;
	while ((LPC_SYSCTL->SYSPLLSTAT & 1) == 0) {}

	// make sure USB PLL is running
	LPC_SYSCTL->USBPLLCLKSEL = 1;
	LPC_SYSCTL->USBPLLCLKUEN = 0;
	LPC_SYSCTL->USBPLLCLKUEN = 1;
	LPC_SYSCTL->PDRUNCFG &= ~(1 << 8);
	while ((LPC_SYSCTL->USBPLLSTAT & 1) == 0) {}

	LPC_SYSCTL->MAINCLKSEL = 0x3;
	LPC_SYSCTL->MAINCLKUEN = 0;
	LPC_SYSCTL->MAINCLKUEN = 1;

	LPC_SYSCTL->USBCLKSEL = 1;
	LPC_SYSCTL->USBCLKUEN = 0;
	LPC_SYSCTL->USBCLKUEN = 1;
	LPC_SYSCTL->USBCLKDIV = 1;

	// --- USB controller initialization ---
	// pins
	LPC_SYSCTL->SYSAHBCLKCTRL |= (1 << 16);
	LPC_IOCON->REG[VCCBUS] = FUNC1;
	LPC_IOCON->REG[USBCONNECT] = FUNC1;

	// power for USB
	LPC_SYSCTL->SYSAHBCLKCTRL |= (1 << 14);
	LPC_SYSCTL->PDRUNCFG &= ~(1 << 10);

	uint8_t counter = 75;
	while(counter) {
		__NOP();
		counter--;
	}

	// set interrupts
	LPC_USB->DevIntEn = ENDPOINTS_ENABLED_FOR_INTERRUPTS;

	writeToSIE(SET_ADDRESS_COMMAND, (ADDRESS(0) | DEVICE_ENABLED));
	writeToSIE(SET_MODE, ALWAYS_PLL_CLOCK_NAK_INTERRUPTS_FOR_NONCONTROL_IN_AND_OUT);
	writeToSIE(SET_DEVICE_STATUS, CONNECT);

	NVIC_EnableIRQ(USB0_IRQn);
}

void writeData(uint32_t data) {
	LPC_USB->DevIntClr |= CC_EMPTY_BIT;
	LPC_USB->CmdCode = 0x00800100;
	while(!(LPC_USB->DevIntSt & CC_EMPTY_BIT)) {}
}
