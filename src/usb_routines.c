/*
 * usb_routines.c
 *
 *  Created on: Dec 2, 2014
 *      Author: denismasyukov
 */
#include "usb_definitions.h"

void writeToSIE(uint32_t command, uint32_t data) {
	LPC_USB->DevIntClr |= CC_EMPTY_BIT;
	LPC_USB->CmdCode = command;
	while(!(LPC_USB->DevIntSt & CC_EMPTY_BIT)) {}

	LPC_USB->DevIntClr |= CC_EMPTY_BIT;
	LPC_USB->CmdCode = data;
	while(!(LPC_USB->DevIntSt & CC_EMPTY_BIT)) {}
}

void writeToSIEWithoutData(uint32_t command) {
	LPC_USB->DevIntClr |= CC_EMPTY_BIT;
	LPC_USB->CmdCode = command;
	while(!(LPC_USB->DevIntSt & CC_EMPTY_BIT)) {}
}

uint8_t readFromSIE(uint8_t command) {
	LPC_USB->DevIntClr |= CC_EMPTY_BIT;
	LPC_USB->CmdCode = SIE_COMMAND(command);
	while(!(LPC_USB->DevIntSt & CC_EMPTY_BIT)) {}
	LPC_USB->DevIntClr |= CC_FULL_BIT;
	LPC_USB->CmdCode = SIE_READ(command);
	while(!(LPC_USB->DevIntSt & CC_FULL_BIT)) {}
	return LPC_USB->CmdData;
}
