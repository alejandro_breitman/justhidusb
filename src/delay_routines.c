/*
 * delay_routines.c
 *
 *  Created on: Nov 20, 2014
 *      Author: denismasyukov
 */

#include "stdint.h"
#include "chip.h"

void delayMicroseconds(uint32_t count) {
	uint8_t CYCLES_IN_MICROSECOND = (Chip_Clock_GetSystemClockRate() / 1000000);

	// enable clocking
	LPC_SYSCTL->SYSAHBCLKCTRL |= (1 << 7);

	// reset TC and PC
	LPC_TIMER16_0->TCR = (1 << 1);

	// Set prescaler and match registers
	LPC_TIMER16_0->PR = CYCLES_IN_MICROSECOND;
	LPC_TIMER16_0->MR[0] = count;
	// Stop on MR0
	LPC_TIMER16_0->MCR |= (1 << 2);
	// enable counter
	LPC_TIMER16_0->TCR = 1;

	// wait
	while (LPC_TIMER16_0->TCR != 0) {}

	LPC_SYSCTL->SYSAHBCLKCTRL &= ~(1 << 7);
}

void delayMilliseconds(uint16_t millis) {
	uint16_t count = millis;
	while(count) {
		delayMicroseconds(1000);
		count--;
	}
}
