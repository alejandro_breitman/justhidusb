/*
 * usb_routines.h
 *
 *  Created on: Dec 2, 2014
 *      Author: denismasyukov
 */

#ifndef USB_ROUTINES_H_
#define USB_ROUTINES_H_

void writeToSIE(uint32_t command, uint32_t data);
void writeToSIEWithoutData(uint32_t command);
uint8_t readFromSIE(uint32_t command);

#endif /* USB_ROUTINES_H_ */
