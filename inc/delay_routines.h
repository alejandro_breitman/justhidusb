/*
 * delay_routines.h
 *
 *  Created on: Nov 20, 2014
 *      Author: denismasyukov
 */

#ifndef DELAY_ROUTINES_H_
#define DELAY_ROUTINES_H_

#include "stdint.h"

void delayMicroseconds(uint32_t count);
void delayMilliseconds(uint16_t millis);

#endif /* DELAY_ROUTINES_H_ */
