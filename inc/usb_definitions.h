#ifndef __USB_DEFINITIONS
#define __USB_DEFINITIONS

#include "chip.h"

typedef struct
{
  __I  uint32_t DevIntSt;            /* USB Device Interrupt Registers     */
  __IO uint32_t DevIntEn;
  __O  uint32_t DevIntClr;
  __O  uint32_t DevIntSet;

  __O  uint32_t CmdCode;             /* USB Device SIE Command Registers   */
  __I  uint32_t CmdData;

  __I  uint32_t RxData;              /* USB Device Transfer Registers      */
  __O  uint32_t TxData;
  __I  uint32_t RxPLen;
  __O  uint32_t TxPLen;
  __IO uint32_t Ctrl;
  __O  uint32_t DevFIQSel;
} LPC_USB_TypeDef;

#define LPC_APB0_BASE         (0x40000000UL)
#define LPC_USB_BASE          (LPC_APB0_BASE + 0x20000)
#define LPC_USB               ((LPC_USB_TypeDef    *) LPC_USB_BASE   )

#define UPPER_HALF_OF_WORD(x)	((x & 0xFF00) >> 8)
#define CC_EMPTY_BIT		  	(1 << 10)
#define CC_FULL_BIT			  	(1 << 11)
#define READ_MODE				(1 << 0)
#define WRITE_MODE				(1 << 1)
#define LOGICAL_ENDPOINT(x)		(x << 2)
#define DATA_VALID_BIT			(1 << 10)

#define LOGICAL_ENDPOINT(x)		(x << 2)
#define CONTROL_OUT_ENDPOINT	0
#define CONTROL_IN_ENDPOINT		1

#define SIE_COMMAND(x)													(0x00000500 | (x << 16))
#define SIE_READ(x)														(0x00000200 | (x << 16))

#define SIE_ERROR_CODE_COMMAND											0xFF
#define CLEAR_BUFFER_COMMAND											0xF2
#define SELECT_ENDPOINT_COMMAND											0x40
#define VALIDATE_BUFFER_COMMAND_CODE									0x00FA0500
#define SET_MODE														0x00F30500
#define SET_ADDRESS_COMMAND 											0x00D00500
#define SET_DEVICE_STATUS												0x00FE0500
#define ENABLE_NON_CONTROL_ENDPOINTS									0x00D80500

#define ALWAYS_PLL_CLOCK_NAK_INTERRUPTS_FOR_NONCONTROL_IN_AND_OUT		0x00190100
#define DEVICE_ENABLED													0x00800100
#define CONNECT															0x00010100
#define SELECT_ENDPOINT_0_COMMAND_CODE         							0x00000500
#define ADDRESS(x)														(0x00000100 | (x << 16))
#define CONFIGURE_DEVICE												0x00010100

#define TxENDPKT						(1 << 13)

#define DEVICE_TO_HOST			0x80
#define HOST_TO_DEVICE				0x0
#define GET_DESCRIPTOR					0x6
#define DEVICE_DESCRIPTOR_TYPE			0x1
#define CONFIGURATION_DESCRIPTOR_TYPE	0x2
#define SET_ADDRESS						0x5
#define CLEAR_FEATURE					0x1
#define SET_CONFIGURATION				0x9
#define SET_DESCRIPTOR					0x7
#define SET_FEATURE						0x3
#define SET_IDLE						0xA
#define GET_CONFIGURATION				0x8
#define GET_INTERFACE					0xA
#define	GET_STATUS						0x0

#define CONTROL_ENDPOINT				(1 << 1)

#define EP0								(1 << 1)
#define EP1								(1 << 2)
#define EP2								(1 << 3)
#define EP3								(1 << 4)

#define NAK_SENT_OR_RECEIVED			9

#define GET_HID_REPORT_DESCRIPTOR				0x22
#define LOGICAL_HID_ENDPOINT					0x1
#define LOGICAL_CONTROL_ENDPOINT				0x0

#define ERROR_ACTIVE_BIT						(1 << 4)

#define INTEFRACE_TO_HOST		0x81

#define SETUP_BIT								(1 << 2)

#define LOGICAL_IN_ENDPOINT_1					(1 << 4)
#define CONTROL_ENDPOINT_OUT					(1 << 1)




#endif
